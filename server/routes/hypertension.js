const express = require("express");

const router = express.Router();
const hypertensionController = require("../controllers/hypertensionController");

// Route to get the hypertension calculator result
router.post(
  "/classification",
  hypertensionController.hypertensionClassification
);

module.exports = router;
