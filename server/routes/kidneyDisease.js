const express = require("express");
const kidneyController = require("../controllers/kidneyController");

const router = express.Router();

// Route to get the hypertension calculator result
router.post("/classification", kidneyController.kidneyClassification);

module.exports = router;
