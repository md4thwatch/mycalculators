/*************************
 * IMPORTS AND INITIALIZATION
 *************************/
// Import modules
const express = require("express");
const cors = require("cors");

const hypertensionRouter = require("./routes/hypertension");
const kidneyRouter = require("./routes/kidneyDisease");

// Initialize express app
const app = express();

/*************************
 * MIDDLEWARE
 ************************/
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

/************************
 * ROUTES
 ************************/
app.use("/hypertension", hypertensionRouter);
app.use("/kidney", kidneyRouter);

// Export main application
module.exports = app;
