const logic = require("../utils/logic");

exports.hypertensionClassification = (req, res) => {
  let bodyCopy = [...req.body];
  let classString;

  //Convert array to dates
  logic.convertToDates(bodyCopy);

  // Sort array by dates
  logic.sortByDate(bodyCopy);

  // Calculate the result
  classString = logic.calculateHypertensionResult(bodyCopy);

  res.status(200).json({
    success: true,
    data: {
      sortedArray: bodyCopy,
      classString,
    },
  });
};
