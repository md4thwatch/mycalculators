const logic = require("../utils/logic");

exports.kidneyClassification = (req, res) => {
  let bodyCopy = [...req.body];

  //Convert array to dates
  logic.convertToDates(bodyCopy);

  // Sort array by dates
  logic.sortByDate(bodyCopy);

  // Calculate the result
  classString = logic.calculateKidneyDiseaseResult(bodyCopy);
  const returnArray = logic.checkConsecutiveDrops(bodyCopy);

  res.status(200).json({
    success: true,
    data: {
      sortedArray: returnArray,
      classString,
    },
  });
};
