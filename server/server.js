/*************************
 * IMPORTS AND INITIALIZATION
 *************************/
// Imports
const app = require("./app");

// Assign port number to variable
const PORT = 5000;

// Create a server
const server = app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
