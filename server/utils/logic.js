// Functions converts date strings to date objects
exports.convertToDates = (inputArray) => {
  inputArray.forEach((el, index) => {
    // Convert date strings to date objects
    el.atDate = new Date(el.atDate);
  });
};

// Sorts array by the atDate property
exports.sortByDate = (inputArray) => {
  inputArray.sort((a, b) => {
    return new Date(a.atDate) - new Date(b.atDate);
  });
};

exports.calculateHypertensionResult = (inputArray) => {
  // Since array will already be sorted before passing the array the last element in the array is most recent
  let patientRecord = inputArray[inputArray.length - 1];

  if (patientRecord.SysBP >= 180 && patientRecord.DiaBP >= 120) {
    return "Stage 3";
  } else if (
    (patientRecord.SysBP >= 160 && patientRecord.SysBP < 180) ||
    (patientRecord.DiaBP >= 100 && patientRecord.DiaBP < 110)
  ) {
    return "Stage 2";
  } else if (
    (patientRecord.SysBP >= 140 && patientRecord.SysBP < 160) ||
    (patientRecord.DiaBP >= 90 && patientRecord.DiaBP < 100)
  ) {
    return "Stage 1";
  } else {
    return "No Hypertension";
  }
};

exports.calculateKidneyDiseaseResult = (inputArray) => {
  let patientRecord = inputArray[inputArray.length - 1];

  if (patientRecord.eGFR >= 90) {
    return "Normal";
  } else if (patientRecord.eGFR >= 60 && patientRecord.eGFR <= 89) {
    return "Mildly Decreased";
  } else if (patientRecord.eGFR >= 45 && patientRecord.eGFR <= 59) {
    return "Mild to Moderate";
  } else if (patientRecord.eGFR >= 30 && patientRecord.eGFR <= 44) {
    return "Moderate to Severe";
  } else if (patientRecord.eGFR >= 15 && patientRecord.eGFR <= 29) {
    return "Severely Decreased";
  } else {
    return "Kidney Failure";
  }
};

const getPercentageDrop = (recent, before) => {
  //a is the most recent date b is the one before
  // make sure that a is less than b which means
  // there was a drop
  if (Number(recent) < Number(before)) {
    return parseInt((1 - recent / before) * 100);
  }
};

exports.checkConsecutiveDrops = (inputArray) => {
  let newArr = [];
  inputArray.forEach((el, index, arr) => {
    let percentageDrop = "";

    if (index !== 0) {
      percentageDrop = getPercentageDrop(el.eGFR, inputArray[index - 1].eGFR);
    }

    newArr.push({ ...el, percentageDrop });
  });

  // return newArr;
  return newArr;
};
