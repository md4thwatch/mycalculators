1. Clone Repo 
2. Open Client folder and run "npm install" to install packages, same as in the Server folder, run "npm install".
3. Open a separate terminal for the client folder and also the server folder
4. In the client folder run "npm start" - Default port is 3000
5. In the server folder run "node server" = Default port is 5000
6. Open browser and navigate to http://localhost:3000
7. Test the app and have fun!