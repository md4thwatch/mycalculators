import validateDate from "validate-date";

// helper function for dateFormatValidator()
export const validDateFormat = (date) => {
  let valid = true;
  //check if string is valid length
  if (date.length !== 10) {
    valid = false;
  } else {
    // if valid length check each of the values
    // check if indexes 4 and 7 equal '/'
    if (date[4] === "/" && date[7] === "/") {
      //check if all the other indexes are numbers
      for (let i = 0; i < date.length; i++) {
        if (!(i === 4 || i === 7)) {
          // check if each character is a number
          valid = isNaN(date.charAt(i)) ? false : true;
        }
      }
    } else {
      valid = false;
    }
  }

  // return the valid variable to see if valid
  return valid;
};

// dateFormat validator, validates that the array dates is properly formatted as YYYY/MM/DD
// Parameter(s) - accepts one (array)
// Return - returns true or false
export const dateFormatValidator = (inputArray) => {
  // CYcle through each element in the array
  inputArray.forEach((el, index, arr) => {
    // get the atDate property from each element
    if (!validDateFormat(el.atDate)) {
      return false;
    }
  });
  return true;
};

export const dateValidator = (date) => {
  let valid = true;
  if (!validateDate(date, "boolean", "yyyy/mm/dd")) {
    valid = false;
  }
  return valid;
};
