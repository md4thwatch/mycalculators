// This will sort the array objects based on date
const sortArrayByDate = (inputArray) => {
  inputArray.sort((a, b) => {
    return new Date(a.atDate) - new Date(b.atDate);
  });
};
