import React, { Component } from "react";
import axios from "axios";

import KidneyDiseaseDiv from "./KidneyDiseaseStyle";
import Textfield from "../Textfield/Textfield";
import Button from "../Button/Button";
import { dateValidator, validDateFormat } from "../../utils/Validator";

export class KidneyDisease extends Component {
  constructor(props) {
    super(props);
    this.state = {
      eGFRInput: "",
      eGFRInputError: false,
      eGFRInputErrorText: "",
      dateInput: "",
      dateInputError: false,
      dateInputErrorText: "",
      list: [],
      listFromServer: [{}],
      disableCalculate: true,
      calculateBtnText: "Calculate",
      lastReading: null,
      class: "",
    };
  }

  // This handles the input change
  inputDataChangeHandler = (e, input) => {
    this.setState({ [input]: e.target.value });
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.state.list.length !== prevState.list.length) {
      this.setState({ disableCalculate: false });
    }
  }

  //format array
  formatArrayElement = () => {
    let obj = {
      eGFR: this.state.eGFRInput,
      atDate: new Date(this.state.dateInput),
    };

    // copy state list
    let stateList = [...this.state.list];
    stateList.push(obj);

    this.setState({ list: stateList });
  };

  // onCLickHandler - adds to list, but validates all inputs first
  addBtnHandler = () => {
    // Reset error state defaults, in case this is the second time inputting data from error
    this.setState({
      eGFRInputError: false,
      dateInputError: false,
    });

    //validate each value
    if (isNaN(this.state.eGFRInput.trim())) {
      return this.setState({
        eGFRInputError: true,
        eGFRInputErrorText: "Not a valid number",
      });
    }

    // CHeck that date is properly formatted
    if (!validDateFormat(this.state.dateInput.trim())) {
      return this.setState({
        dateInputError: true,
        dateInputErrorText: "Not a valid date",
      });
    }

    // Check that date is an acutal valid date
    if (!dateValidator(this.state.dateInput.trim())) {
      return this.setState({
        dateInputError: true,
        dateInputErrorText: "Not a valid date",
      });
    }

    // Formats the array and adds to the state 'list'
    this.formatArrayElement();
  };

  // clearBtnHandler - clears list
  clearBtnHandler = () => {
    this.setState({ list: [] });
  };

  // Gets data array and sends to database, where it is processed and returned back
  calculateBtnHandler = () => {
    this.setState({ disableCalculate: true, calculateBtnText: "Please Wait" });

    axios({
      method: "post",
      url: "http://localhost:5000/kidney/classification",
      data: this.state.list,
    })
      .then((res) => {
        let data = res.data.data.sortedArray;
        console.log(data);
        this.setState({
          listFromServer: data,
          class: res.data.data.classString,
        });
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        this.setState({
          calculateBtnText: "Calculate",
          disableCalculate: false,
        });
      });
  };

  renderPercentageDrop = () => {
    return this.state.listFromServer.map((el, index, arr) => {
      if (parseInt(el.percentageDrop) >= 20) {
        return (
          <div className="overall">
            <div className="recentDate">
              <h4>eGFR: {el.eGFR}</h4>
              <h4>eGFR: {new Date(el.atDate).toLocaleDateString()}</h4>
            </div>
            <h5>Dropped From:</h5>
            <div className="beforeDate">
              <h4>eGFR: {arr[index - 1].eGFR}</h4>
              <h4>
                eGFR: {new Date(arr[index - 1].atDate).toLocaleDateString()}
              </h4>
            </div>
            <hr />
          </div>
        );
      }
      return null;
    });
  };

  render() {
    return (
      <KidneyDiseaseDiv>
        <h2 className="title">Kidney Disease Calculator</h2>
        <div className="kidneyDiseaseCard">
          <div className="topBar"></div>
          <div className="contentSection">
            <div className="bp">
              <div className="eGFR">
                <Textfield
                  text="eGFR"
                  inputProps={{
                    value: this.state.eGFRInput,
                    onChange: (e) =>
                      this.inputDataChangeHandler(e, "eGFRInput"),
                  }}
                  errorText={this.state.eGFRInputErrorText}
                  error={this.state.eGFRInputError}
                />
              </div>
            </div>
            <div className="dateInput">
              <Textfield
                text="Enter date - must be in YYYY/MM/DD format"
                inputProps={{
                  value: this.state.dateInput,
                  onChange: (e) => this.inputDataChangeHandler(e, "dateInput"),
                }}
                errorText={this.state.dateInputErrorText}
                error={this.state.dateInputError}
              />
            </div>
            <div className="btnSection">
              <Button onClick={this.addBtnHandler}>Add To List</Button>
            </div>
            <div className="btnSection">
              <Button onClick={this.clearBtnHandler}>Clear List</Button>
            </div>
            <div className="list">
              {this.state.list.map((el) => (
                <div className="record">
                  <div>eGFR: {el.eGFR}</div>
                  <div>Date: {new Date(el.atDate).toDateString()}</div>
                </div>
              ))}
            </div>
            <div className="btnSection">
              <Button
                onClick={this.calculateBtnHandler}
                buttonProps={{ disabled: this.state.disableCalculate }}
              >
                {this.state.calculateBtnText}
              </Button>
            </div>
            <div className="resultSection">
              <div className="recordRecent">
                <h4>
                  eGFR:{" "}
                  {this.state.listFromServer[
                    this.state.listFromServer.length - 1
                  ]
                    ? this.state.listFromServer[
                        this.state.listFromServer.length - 1
                      ].eGFR
                    : null}
                </h4>
                <h4>
                  Date:{" "}
                  {new Date(
                    this.state.listFromServer[
                      this.state.listFromServer.length - 1
                    ].atDate
                  ).toLocaleDateString() !== "Invalid Date"
                    ? new Date(
                        this.state.listFromServer[
                          this.state.listFromServer.length - 1
                        ].atDate
                      ).toLocaleDateString()
                    : null}
                </h4>
                <h4>Class: {this.state.class ? this.state.class : null}</h4>
              </div>

              <div className="percentDrops">
                <h4>Percentage Drops More Than Or Equal To 20%</h4>
                {this.renderPercentageDrop()}
              </div>
            </div>
          </div>
        </div>
      </KidneyDiseaseDiv>
    );
  }
}

export default KidneyDisease;
