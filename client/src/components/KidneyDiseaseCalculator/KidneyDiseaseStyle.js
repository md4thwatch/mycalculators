import styled from "styled-components";

const KidneyDiseaseDiv = styled.div`
  width: 500px;

  * {
    font-family: "Asap", Helvetica, Arial, Lucida, sans-serif;
    box-sizing: border-box;
  }

  .title {
    margin: 0;
    text-align: left;
    font-weight: 500;
    color: #715dd7;
  }

  .kidneyDiseaseCard {
    box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
    border-radius: 15px;
    margin-top: 20px;
    overflow: hidden;

    .topBar {
      width: 100%;
      height: 20px;
      background: linear-gradient(to right, #cecbef, #e6eaf6);
    }

    .contentSection {
      padding: 25px;

      .bp {
        margin-bottom: 20px;
      }

      .dateInput {
        margin-bottom: 20px;
      }

      .innerTitle {
        margin: 0;
        margin-bottom: 10px;
        text-align: left;
        font-size: 20px;
        color: #343a40;
      }

      .btnSection {
        margin-top: 20px;
        margin-bottom: 20px;
      }

      .resultSection {
        /* display: flex;
        flex-direction: column; */
        color: #343a40;
      }

      .percentDrops {
        height: 300px;
        background: white;
        overflow: scroll;
        border-radius: 15px;
        display: flex;
        flex-direction: column;
        align-items: center;

        .overall {
          width: 100%;
        }

        .recentDate {
          background: #715dd7;
          color: white;
          border-radius: 15px;
          padding: 10px;

          h4 {
            margin: 0;
          }
        }

        .beforeDate {
          background: #715dd7;
          color: white;
          border-radius: 15px;
          padding: 10px;

          h4 {
            margin: 0;
          }
        }

        hr {
          width: 100%;
          height: 3px;
          background: black;
          outline: none;
          margin-top: 20px;
          margin-bottom: 20px;
        }
      }

      .list {
        background: white;
        border-radius: 10px;
        height: 100px;
        overflow: scroll;

        .record {
          display: flex;
          justify-content: space-between;
        }
      }

      .classificationText {
        font-size: 18px;
        margin: 0;
        text-align: left;
      }
    }
  }
`;

export default KidneyDiseaseDiv;
