import React, { Component } from "react";

import MainDiv from "./MainStyle";
import Hypertension from "./HypertensionCalculator/Hypertension";
import KidneyDisease from "./KidneyDiseaseCalculator/KidneyDisease";

export class Main extends Component {
  render() {
    return (
      <MainDiv>
        <div className="section">
          <Hypertension />
        </div>
        <div className="section">
          <KidneyDisease />
        </div>
      </MainDiv>
    );
  }
}

export default Main;
