import styled from "styled-components";
//#CECBEF
//#E6EAF6

const HypertensionDiv = styled.div`
  width: 500px;

  * {
    font-family: "Asap", Helvetica, Arial, Lucida, sans-serif;
    box-sizing: border-box;
  }

  .title {
    margin: 0;
    text-align: left;
    font-weight: 500;
    color: #715dd7;
  }

  .hypertensionCard {
    box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
    border-radius: 15px;
    margin-top: 20px;
    overflow: hidden;

    .topBar {
      width: 100%;
      height: 20px;
      background: linear-gradient(to right, #cecbef, #e6eaf6);
    }

    .contentSection {
      padding: 25px;

      .bp {
        display: flex;
        justify-content: space-between;

        .sysBp {
          margin-bottom: 20px;
          width: 49%;
        }

        .diaBp {
          margin-bottom: 20px;
          width: 49%;
        }
      }

      .dateInput {
        margin-bottom: 20px;
      }

      .innerTitle {
        margin: 0;
        margin-bottom: 10px;
        text-align: left;
        font-size: 20px;
        color: #343a40;
      }

      .btnSection {
        margin-top: 20px;
        margin-bottom: 20px;
      }

      .resultSection {
        /* display: flex;
        flex-direction: column; */
        color: #343a40;
      }

      .list {
        background: white;
        border-radius: 10px;
        height: 100px;
        overflow: scroll;

        .record {
          display: flex;
          justify-content: space-between;
        }
      }

      .classificationText {
        font-size: 18px;
        margin: 0;
        text-align: left;
      }
    }
  }
`;

export default HypertensionDiv;
