import React, { Component } from "react";
import axios from "axios";

import HypertensionDiv from "./HypertensionStyle";
import Textfield from "../Textfield/Textfield";
import Button from "../Button/Button";
import { dateValidator, validDateFormat } from "../../utils/Validator";

export class Hypertension extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sysInput: "",
      sysInputError: false,
      sysInputErrorText: "",
      diaInput: "",
      diaInputError: false,
      diaInputErrorText: "",
      dateInput: "",
      dateInputError: false,
      dateInputErrorText: "",
      list: [],
      disableCalculate: true,
      calculateBtnText: "Calculate",
      lastReading: null,
      class: "",
    };
  }

  // This handles the input change
  inputDataChangeHandler = (e, input) => {
    this.setState({ [input]: e.target.value });
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.state.list.length !== prevState.list.length) {
      this.setState({ disableCalculate: false });
    }
  }

  //format array
  formatArrayElement = () => {
    let obj = {
      SysBP: parseInt(this.state.sysInput),
      DiaBP: parseInt(this.state.diaInput),
      atDate: new Date(this.state.dateInput),
    };

    // copy state list
    let stateList = [...this.state.list];
    stateList.push(obj);

    this.setState({ list: stateList });
  };

  // onCLickHandler - adds to list, but validates all inputs first
  addBtnHandler = () => {
    // Reset error state defaults, in case this is the second time inputting data from error
    this.setState({
      sysInputError: false,
      diaInputError: false,
      dateInputError: false,
    });

    //validate each value
    if (isNaN(this.state.sysInput.trim())) {
      return this.setState({
        sysInputError: true,
        sysInputErrorText: "Not a valid number",
      });
    }

    if (isNaN(this.state.diaInput.trim())) {
      return this.setState({
        diaInputError: true,
        diaInputErrorText: "Not a valid number",
      });
    }

    // CHeck that date is properly formatted
    if (!validDateFormat(this.state.dateInput.trim())) {
      return this.setState({
        dateInputError: true,
        dateInputErrorText: "Not a valid date",
      });
    }

    // Check that date is an acutal valid date
    if (!dateValidator(this.state.dateInput.trim())) {
      return this.setState({
        dateInputError: true,
        dateInputErrorText: "Not a valid date",
      });
    }

    // Formats the array and adds to the state 'list'
    this.formatArrayElement();
  };

  // clearBtnHandler - clears list
  clearBtnHandler = () => {
    this.setState({ list: [] });
  };

  // Gets data array and sends to database, where it is processed and returned back
  calculateBtnHandler = () => {
    this.setState({ disableCalculate: true, calculateBtnText: "Please Wait" });

    axios({
      method: "post",
      url: "http://localhost:5000/hypertension/classification",
      data: this.state.list,
    })
      .then((res) => {
        let data = res.data.data.sortedArray;
        this.setState({
          lastReading: data[data.length - 1],
          class: res.data.data.classString,
        });
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        this.setState({
          calculateBtnText: "Calculate",
          disableCalculate: false,
        });
      });
  };

  render() {
    return (
      <HypertensionDiv>
        <h2 className="title">Hypertension Calculator</h2>
        <div className="hypertensionCard">
          <div className="topBar"></div>
          <div className="contentSection">
            {/* <h5 className="innerTitle">Hypertension</h5> */}
            <div className="bp">
              <div className="sysBp">
                <Textfield
                  text="SysBP"
                  inputProps={{
                    value: this.state.sysInput,
                    onChange: (e) => this.inputDataChangeHandler(e, "sysInput"),
                  }}
                  errorText={this.state.sysInputErrorText}
                  error={this.state.sysInputError}
                />
              </div>
              <div className="diaBp">
                <Textfield
                  text="DiaBP"
                  inputProps={{
                    value: this.state.diaInput,
                    onChange: (e) => this.inputDataChangeHandler(e, "diaInput"),
                  }}
                  errorText={this.state.diaInputErrorText}
                  error={this.state.diaInputError}
                />
              </div>
            </div>
            <div className="dateInput">
              <Textfield
                text="Enter date - must be in YYYY/MM/DD format"
                inputProps={{
                  value: this.state.dateInput,
                  onChange: (e) => this.inputDataChangeHandler(e, "dateInput"),
                }}
                errorText={this.state.dateInputErrorText}
                error={this.state.dateInputError}
              />
            </div>
            <div className="btnSection">
              <Button onClick={this.addBtnHandler}>Add To List</Button>
            </div>
            <div className="btnSection">
              <Button onClick={this.clearBtnHandler}>Clear List</Button>
            </div>
            <div className="list">
              {this.state.list.map((el) => (
                <div className="record">
                  <div>SysBp: {el.SysBP}</div>
                  <div>DiaBp: {el.DiaBP}</div>
                  <div>Date: {el.atDate.toDateString()}</div>
                </div>
              ))}
            </div>
            <div className="btnSection">
              <Button
                onClick={this.calculateBtnHandler}
                buttonProps={{ disabled: this.state.disableCalculate }}
              >
                {this.state.calculateBtnText}
              </Button>
            </div>
            <div className="resultSection">
              <h4>
                SysBP:{" "}
                {this.state.lastReading ? this.state.lastReading.SysBP : null}
              </h4>
              <h4>
                DiaBP:{" "}
                {this.state.lastReading ? this.state.lastReading.DiaBP : null}
              </h4>
              <h4>
                Date:{" "}
                {this.state.lastReading
                  ? new Date(this.state.lastReading.atDate).toLocaleDateString()
                  : null}
              </h4>
              <h4>Class: {this.state.class ? this.state.class : null}</h4>
            </div>
          </div>
        </div>
      </HypertensionDiv>
    );
  }
}

export default Hypertension;
