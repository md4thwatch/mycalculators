import styled from "styled-components";

const MainDiv = styled.div`
  padding: 50px;
  min-height: 100vh;
  height: 100%;
  box-sizing: border-box;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;

  .section {
    margin: 10px;
    margin-bottom: 50px;
  }
`;

export default MainDiv;
