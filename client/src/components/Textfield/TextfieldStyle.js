import styled from "styled-components";

const TextfieldDiv = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  width: 100%;

  * {
    font-family: "Asap", Helvetica, Arial, Lucida, sans-serif;
    color: #343a40;
    box-sizing: border-box;
  }

  .textfieldLabel {
    font-weight: 600;
    margin-bottom: 3px;
  }

  .textInputDiv {
    border: #715dd7 solid 3px;
    border-radius: 10px;
    overflow: hidden;
    padding: 10px;
    height: ${({ textarea }) => (textarea ? "100px" : "100%")};
    width: 100%;
    background: white;

    input {
      border: none;
      outline: none;
      height: 100%;
      width: 100%;
      font-family: "Asap", Helvetica, Arial, Lucida, sans-serif;
      font-size: 18px;
    }

    input:focus {
    }

    textarea {
      border: none;
      outline: none;
      width: 100%;
      height: 100%;
      font-family: "Asap", Helvetica, Arial, Lucida, sans-serif;
      resize: none;
      font-size: 18px;
    }

    textarea:focus {
    }
  }

  .errorText {
    visibility: ${({ error }) => (error ? "visible" : "hidden")};
    color: #cd3069;
  }
`;

export default TextfieldDiv;
