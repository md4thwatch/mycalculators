import React, { Component } from "react";

import TextfieldDiv from "./TextfieldStyle";

export class Textfield extends Component {
  render() {
    return (
      <TextfieldDiv textarea={this.props.textarea} error={this.props.error}>
        <label className="textfieldLabel">{this.props.text}</label>
        <div className="textInputDiv">
          {this.props.textarea ? (
            <textarea {...this.props.inputProps}></textarea>
          ) : (
            <input type="text" {...this.props.inputProps} />
          )}
        </div>
        <label className="errorText">{this.props.errorText}</label>
      </TextfieldDiv>
    );
  }
}

export default Textfield;
