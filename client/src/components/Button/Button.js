import React, { Component } from "react";

import ButtonButton from "./ButtonStyle";

export class Button extends Component {
  render() {
    return (
      <ButtonButton onClick={this.props.onClick} {...this.props.buttonProps}>
        {this.props.children}
      </ButtonButton>
    );
  }
}

export default Button;
