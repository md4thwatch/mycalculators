import styled from "styled-components";

const ButtonButton = styled.button`
  width: 100%;
  box-sizing: border-box;
  padding: 15px 25px;
  outline: 0;
  color: white;
  font-size: 18px;
  background: #715dd7;
  /* border: #715dd7 solid 3px; */
  outline: none;
  border: none;
  border-radius: 10px;
  font-weight: 700;
  transition: background-color 0.3s ease-in-out;

  &:hover {
    border: none;
    background: #37115c;
    cursor: pointer;
  }
`;

export default ButtonButton;
